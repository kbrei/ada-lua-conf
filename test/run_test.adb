with Ada.Text_IO; use Ada.Text_IO;
with Lua_Conf; use Lua_Conf;

function Run_Test return Integer
is
   Config : Lua_Config;
   Number : Long_Float;
   Numbers : Lua_Conf.Long_Floats.Vector;

   procedure Dump is
   begin
      for Err of Errors (Config) loop
         Put_Line (Err.Message);
      end loop;
   end Dump;
begin
   Config := Load ("test/test.lua");
   Number := Get (Config, "float", 42.0);
   Numbers := Get (Config, "nested.foo.bar", Mandatory => True);

   Put_Line ("starting..");
   Put_Line (Long_Float'Image (Number));

   for Num of Numbers loop
      Put_Line (Long_Float'Image (Num));
   end loop;

   Clear (Config, "x.y");

   declare
      Foo : constant String := Get (Config, "x.y");
   begin
      Put_Line (Foo);
   end;

   Dump;
   return 1;
end Run_Test;
