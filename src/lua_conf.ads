with Lua; use Lua;

with Ada.Containers.Vectors;
with Ada.Containers.Indefinite_Vectors;

--  @summary
--  Read a lua script as Config 
--
--  @description
--  This package provides routine for extracting
--  global varaibles out of a lua script.
package Lua_Conf is

   pragma Assertion_Policy (Check);

   --  Abstract Lua_Config Type
   --  Each Get Works on one Lua_Config
   --  To create a Lua_Config, call Load.
   --  To destroy it call Close after you are done.
   type Lua_Config is private;

   function State (Config : Lua_Config) return Lua_State;

   --  Possible errors which can occur during the Get function
   --  These errors will be collected are accessable via the Error function.
   --  @value Requested variable is of the wrong type. Nil counts as type Nil.
   --  @value Lua_Error a recoverable error manipulating the Lua_State occurred.
   type Error_Type is
     (Type_Mismatch,
      Lua_Error);

   --  This exception is thrown when a Get is called with Mandatory = True,
   --  But no value could be received
   Mandatory_Option_Missing : exception;

   --  This exception is thrown when loading failed.
   --  Possible reasons include:
   --  config file not found,
   --  syntax error,
   --  lua code produced an erro
   Load_Error : exception;

   --  A Error_Message bundled with the Error_Type
   --  @field Message_Length The length of the error message
   --  @field Message The error message
   --  @field Error_Type The type of error which occurred
   type Error_Message (Message_Length : Positive) is record
      Error_Type : Lua_Conf.Error_Type;
      Message : String(1..Message_Length);
   end record;

   ------------------------------------------------------------------
   -- Vector instances for Skalars                                 --
   --  (String, Boolean, Integer, Float, Long_Float, Lua_Function) --
   -- These are used to translate homogenous Lua tables/arrays     --
   ------------------------------------------------------------------

   package Strings is new Ada.Containers.Indefinite_Vectors
     (Index_Type => Positive,
      Element_Type => String);

   package Integers is new Ada.Containers.Vectors
     (Index_Type => Positive,
      Element_Type => Integer);

   package Floats is new Ada.Containers.Vectors
     (Index_Type => Positive,
      Element_Type => Float);

   package Long_Floats is new Ada.Containers.Vectors
     (Index_Type => Positive,
      Element_Type => Long_Float);

   package Booleans is new Ada.Containers.Vectors
     (Index_Type => Positive,
      Element_Type => Boolean);

   package Lua_Functions is new Ada.Containers.Vectors
     (Index_Type => Positive,
      Element_Type => Lua_Function);

   --  All non-critical errors are collected in
   --  a Vector
   package Error_Messages is new Ada.Containers.Indefinite_Vectors
     (Index_Type => Positive,
      Element_Type => Error_Message);

   --  Does nothing
   procedure Nothing (State : Lua_State) is null;

   --  Open all standard libs
   procedure Open_Libs (State : Lua_State);

   --  Open all standard libs which don't do IO operations.
   procedure Open_Non_Io_Libs (State : Lua_State);

   --  This function Loads and runs the .lua file
   --  specified with File_Path.
   --
   --  @param File_Path Path to .lua script
   --
   --  @param Prerun This procedure is executed after loading,
   --  but before running the Lua Script.
   --  This is used to load the Lua standard libs into the script.
   --  Per default this loads all standard libraries that do not allow IO.
   --  These are: coroutine, string, table, utf8 and math
   -- 
   --  @param Postrun Like Prerun, but it will be run after
   --  running the Lua. Per default it does nothing.
   --
   --  @return A Lua_Config where the scipt given by File_Path
   --  is loaded, run and ready for Get method calls
   --
   --  @exception Load_Error an error occurred while
   --  loading the script. Possible reasons include:
   --  File_Path not found, Loading failed, running failed,
   --  Pre or Postrun threw an exception
   function Load
     (File_Path : String;
      Prerun : not null access procedure
        (State : Lua_State) := Open_Non_Io_Libs'Access;
      Postrun : not null access procedure
        (State : Lua_State) := Nothing'Access)
     return Lua_Config;

   --  Closes the Lua_Config.
   --  This cloeses the internal Lua_State and
   --  frees all aggregated error messages.
   --
   --  @param Config Config to be closed
   procedure Close (Config : in out Lua_Config);

   --  Returns all non-fatal erros that occurred in Get functions
   --
   --  @return A vector containing all non-fatal errors of the Get functions
   function Errors (Config : Lua_Config) return Error_Messages.Vector;


   --  This function check wether or not Path
   --  is of the form aaa.bb.cc.dd etc.
   --  A valid Path connot begin or end with separators.
   --  Consecutive seperators are also forbidden.
   --
   --  @return True, when Path conforms to the form above.
   function Is_Path
     (Path : String;
      Separator : Character := '.')
     return Boolean;

   --  Clear a given global value
   procedure Clear
     (Config : in out Lua_Config;
      Name : String);

   --  This generic is usefull to build all the Get methods below.
   --  You can use it to build your own Get functions
   --  for datatypes that are not suppoerted by this package.
   --  Lua_User_Data comes to mind.
   --  All you have to provide is the Result_Type, a Default_Value
   --  and Extract_From_Lua_Top, which decides how do translate
   --  the Lua value currently on the top of the stack into an Ada value.
   generic
      type Result_Type (<>) is private;
      Default_Value : Result_Type;
      with function Extract_From_Lua_Top (State : Lua_State) return Result_Type is <>;
   function Get_Generic
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Result_Type := Default_Value;
      Mandatory : Boolean := False)
      return Result_Type
   with Pre => Is_Path (Field_Path);

   --  Given a Loaded Lua_Config, this extracts
   --  a global variable out of the config script,
   --  given by Field_Path.
   --  Field_Path can index into tables.
   --  Example: table.foo.value
   --  Finds the varaible table, indexes that with foo, which in turn
   --  returns a table which contains value.
   --  The value will then be converted into an Ada type.
   --
   --  @param The Lua_Config to read from
   --
   --  @param Field_Path A dot separated Path, which has to conform to Is_Path
   --
   --  @param Default Default value to use if the Lua
   --  value has the wrong type or doesn't exist
   --
   --  @param Mandatory When True, whenever no value can be retrieved
   --  Mandatory_Option_Missing excetion is thrown.
   --
   --  @exception Mandatory_Option_Missing Thrown when Mandatory = True
   --  and no value can be retrieved
   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Boolean := False;
      Mandatory : Boolean := False)
     return Boolean
   with Pre => Is_Path (Field_Path);

   --  Given a Loaded Lua_Config, this extracts
   --  a global variable out of the config script,
   --  given by Field_Path.
   --  Field_Path can index into tables.
   --  Example: table.foo.value
   --  Finds the varaible table, indexes that with foo, which in turn
   --  returns a table which contains value.
   --  The value will then be converted into an Ada type.
   --
   --  @param The Lua_Config to read from
   --
   --  @param Field_Path A dot separated Path, which has to conform to Is_Path
   --
   --  @param Default Default value to use if the Lua
   --  value has the wrong type or doesn't exist
   --
   --  @param Mandatory When True, whenever no value can be retrieved
   --  Mandatory_Option_Missing excetion is thrown.
   --
   --  @exception Mandatory_Option_Missing Thrown when Mandatory = True
   --  and no value can be retrieved
   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Booleans.Vector := Booleans.Empty_Vector;
      Mandatory : Boolean := False)
     return Booleans.Vector
   with Pre => Is_Path (Field_Path);

   --  Given a Loaded Lua_Config, this extracts
   --  a global variable out of the config script,
   --  given by Field_Path.
   --  Field_Path can index into tables.
   --  Example: table.foo.value
   --  Finds the varaible table, indexes that with foo, which in turn
   --  returns a table which contains value.
   --  The value will then be converted into an Ada type.
   --
   --  @param The Lua_Config to read from
   --
   --  @param Field_Path A dot separated Path, which has to conform to Is_Path
   --
   --  @param Default Default value to use if the Lua
   --  value has the wrong type or doesn't exist
   --
   --  @param Mandatory When True, whenever no value can be retrieved
   --  Mandatory_Option_Missing excetion is thrown.
   --
   --  @exception Mandatory_Option_Missing Thrown when Mandatory = True
   --  and no value can be retrieved
   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : String := "";
      Mandatory : Boolean := False)
     return String
   with Pre => Is_Path (Field_Path);

   --  Given a Loaded Lua_Config, this extracts
   --  a global variable out of the config script,
   --  given by Field_Path.
   --  Field_Path can index into tables.
   --  Example: table.foo.value
   --  Finds the varaible table, indexes that with foo, which in turn
   --  returns a table which contains value.
   --  The value will then be converted into an Ada type.
   --
   --  @param The Lua_Config to read from
   --
   --  @param Field_Path A dot separated Path, which has to conform to Is_Path
   --
   --  @param Default Default value to use if the Lua
   --  value has the wrong type or doesn't exist
   --
   --  @param Mandatory When True, whenever no value can be retrieved
   --  Mandatory_Option_Missing excetion is thrown.
   --
   --  @exception Mandatory_Option_Missing Thrown when Mandatory = True
   --  and no value can be retrieved
   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Strings.Vector := Strings.Empty_Vector;
      Mandatory : Boolean := False)
     return Strings.Vector
   with Pre => Is_Path (Field_Path);

   --  Given a Loaded Lua_Config, this extracts
   --  a global variable out of the config script,
   --  given by Field_Path.
   --  Field_Path can index into tables.
   --  Example: table.foo.value
   --  Finds the varaible table, indexes that with foo, which in turn
   --  returns a table which contains value.
   --  The value will then be converted into an Ada type.
   --
   --  @param The Lua_Config to read from
   --
   --  @param Field_Path A dot separated Path, which has to conform to Is_Path
   --
   --  @param Default Default value to use if the Lua
   --  value has the wrong type or doesn't exist
   --
   --  @param Mandatory When True, whenever no value can be retrieved
   --  Mandatory_Option_Missing excetion is thrown.
   --
   --  @exception Mandatory_Option_Missing Thrown when Mandatory = True
   --  and no value can be retrieved
   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Integer := 0;
      Mandatory : Boolean := False)
     return Integer
   with Pre => Is_Path (Field_Path);

   --  Given a Loaded Lua_Config, this extracts
   --  a global variable out of the config script,
   --  given by Field_Path.
   --  Field_Path can index into tables.
   --  Example: table.foo.value
   --  Finds the varaible table, indexes that with foo, which in turn
   --  returns a table which contains value.
   --  The value will then be converted into an Ada type.
   --
   --  @param The Lua_Config to read from
   --
   --  @param Field_Path A dot separated Path, which has to conform to Is_Path
   --
   --  @param Default Default value to use if the Lua
   --  value has the wrong type or doesn't exist
   --
   --  @param Mandatory When True, whenever no value can be retrieved
   --  Mandatory_Option_Missing excetion is thrown.
   --
   --  @exception Mandatory_Option_Missing Thrown when Mandatory = True
   --  and no value can be retrieved
   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Integers.Vector := Integers.Empty_Vector;
      Mandatory : Boolean := False)
     return Integers.Vector
   with Pre => Is_Path (Field_Path);

   --  Given a Loaded Lua_Config, this extracts
   --  a global variable out of the config script,
   --  given by Field_Path.
   --  Field_Path can index into tables.
   --  Example: table.foo.value
   --  Finds the varaible table, indexes that with foo, which in turn
   --  returns a table which contains value.
   --  The value will then be converted into an Ada type.
   --
   --  @param The Lua_Config to read from
   --
   --  @param Field_Path A dot separated Path, which has to conform to Is_Path
   --
   --  @param Default Default value to use if the Lua
   --  value has the wrong type or doesn't exist
   --
   --  @param Mandatory When True, whenever no value can be retrieved
   --  Mandatory_Option_Missing excetion is thrown.
   --
   --  @exception Mandatory_Option_Missing Thrown when Mandatory = True
   --  and no value can be retrieved
   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Float := 0.0;
      Mandatory : Boolean := False)
     return Float
   with Pre => Is_Path (Field_Path);

   --  Given a Loaded Lua_Config, this extracts
   --  a global variable out of the config script,
   --  given by Field_Path.
   --  Field_Path can index into tables.
   --  Example: table.foo.value
   --  Finds the varaible table, indexes that with foo, which in turn
   --  returns a table which contains value.
   --  The value will then be converted into an Ada type.
   --
   --  @param The Lua_Config to read from
   --
   --  @param Field_Path A dot separated Path, which has to conform to Is_Path
   --
   --  @param Default Default value to use if the Lua
   --  value has the wrong type or doesn't exist
   --
   --  @param Mandatory When True, whenever no value can be retrieved
   --  Mandatory_Option_Missing excetion is thrown.
   --
   --  @exception Mandatory_Option_Missing Thrown when Mandatory = True
   --  and no value can be retrieved
   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Floats.Vector := Floats.Empty_Vector;
      Mandatory : Boolean := False)
     return Floats.Vector
   with Pre => Is_Path (Field_Path);

   --  Given a Loaded Lua_Config, this extracts
   --  a global variable out of the config script,
   --  given by Field_Path.
   --  Field_Path can index into tables.
   --  Example: table.foo.value
   --  Finds the varaible table, indexes that with foo, which in turn
   --  returns a table which contains value.
   --  The value will then be converted into an Ada type.
   --
   --  @param The Lua_Config to read from
   --
   --  @param Field_Path A dot separated Path, which has to conform to Is_Path
   --
   --  @param Default Default value to use if the Lua
   --  value has the wrong type or doesn't exist
   --
   --  @param Mandatory When True, whenever no value can be retrieved
   --  Mandatory_Option_Missing excetion is thrown.
   --
   --  @exception Mandatory_Option_Missing Thrown when Mandatory = True
   --  and no value can be retrieved
   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Long_Float := 0.0;
      Mandatory : Boolean := False)
     return Long_Float
   with Pre => Is_Path (Field_Path);

   --  Given a Loaded Lua_Config, this extracts
   --  a global variable out of the config script,
   --  given by Field_Path.
   --  Field_Path can index into tables.
   --  Example: table.foo.value
   --  Finds the varaible table, indexes that with foo, which in turn
   --  returns a table which contains value.
   --  The value will then be converted into an Ada type.
   --
   --  @param The Lua_Config to read from
   --
   --  @param Field_Path A dot separated Path, which has to conform to Is_Path
   --
   --  @param Default Default value to use if the Lua
   --  value has the wrong type or doesn't exist
   --
   --  @param Mandatory When True, whenever no value can be retrieved
   --  Mandatory_Option_Missing excetion is thrown.
   --
   --  @exception Mandatory_Option_Missing Thrown when Mandatory = True
   --  and no value can be retrieved
   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Long_Floats.Vector := Long_Floats.Empty_Vector;
      Mandatory : Boolean := False)
     return Long_Floats.Vector
   with Pre => Is_Path (Field_Path);

   --  Given a Loaded Lua_Config, this extracts
   --  a global variable out of the config script,
   --  given by Field_Path.
   --  Field_Path can index into tables.
   --  Example: table.foo.value
   --  Finds the varaible table, indexes that with foo, which in turn
   --  returns a table which contains value.
   --  The value will then be converted into an Ada type.
   --
   --  @param The Lua_Config to read from
   --
   --  @param Field_Path A dot separated Path, which has to conform to Is_Path
   --
   --  @param Default Default value to use if the Lua
   --  value has the wrong type or doesn't exist
   --
   --  @param Mandatory When True, whenever no value can be retrieved
   --  Mandatory_Option_Missing excetion is thrown.
   --
   --  @exception Mandatory_Option_Missing Thrown when Mandatory = True
   --  and no value can be retrieved
   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Lua_Function := null;
      Mandatory : Boolean := False)
     return Lua_Function
   with Pre => Is_Path (Field_Path);

   --  Given a Loaded Lua_Config, this extracts
   --  a global variable out of the config script,
   --  given by Field_Path.
   --  Field_Path can index into tables.
   --  Example: table.foo.value
   --  Finds the varaible table, indexes that with foo, which in turn
   --  returns a table which contains value.
   --  The value will then be converted into an Ada type.
   --
   --  @param The Lua_Config to read from
   --
   --  @param Field_Path A dot separated Path, which has to conform to Is_Path
   --
   --  @param Default Default value to use if the Lua
   --  value has the wrong type or doesn't exist
   --
   --  @param Mandatory When True, whenever no value can be retrieved
   --  Mandatory_Option_Missing excetion is thrown.
   --
   --  @exception Mandatory_Option_Missing Thrown when Mandatory = True
   --  and no value can be retrieved
   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Lua_Functions.Vector := Lua_Functions.Empty_Vector;
      Mandatory : Boolean := False)
     return Lua_Functions.Vector
   with Pre => Is_Path (Field_Path);

private
   type Lua_Config is record
      State : Lua_State;
      Error_Vect : Error_Messages.Vector := Error_Messages.Empty_Vector;
   end record;

   function Extract_From_Lua_Top (State : Lua_State) return Boolean;
   function Extract_From_Lua_Top (State : Lua_State) return Booleans.Vector;

   function Extract_From_Lua_Top (State : Lua_State) return String;
   function Extract_From_Lua_Top (State : Lua_State) return Strings.Vector;

   function Extract_From_Lua_Top (State : Lua_State) return Integer;
   function Extract_From_Lua_Top (State : Lua_State) return Integers.Vector;

   function Extract_From_Lua_Top (State : Lua_State) return Float;
   function Extract_From_Lua_Top (State : Lua_State) return Floats.Vector;

   function Extract_From_Lua_Top (State : Lua_State) return Long_Float;
   function Extract_From_Lua_Top (State : Lua_State) return Long_Floats.Vector;

   function Extract_From_Lua_Top (State : Lua_State) return Lua_Function;
   function Extract_From_Lua_Top (State : Lua_State) return Lua_Functions.Vector;

   -- This is defined in such a roundabout way
   -- so that this works
   -- with definite and indefinite Vectors
   generic
      type Result_Type (<>) is private; 
      type Collection is private;

      Empty_Vector : Collection;

      with procedure Append
        (Container : in out Collection;
         New_Item : Result_Type;
         Count : Ada.Containers.Count_Type := 1) is <>;

      with function Extract_From_Lua_Top
        (State : Lua_State) return Result_Type;

   function Extract_Collection_From_Lua_Top
     (State : Lua_State) return Collection;
   
  -- Module specific helpers

   function Create_Message
     (Config : in out Lua_Config;
      File_Path : String;
      Code : Lua_Return_Code)
     return String;

   procedure Collect_Lua_Error
     (Config : in out Lua_Config;
      File_Path : String;
      Code : Lua_Return_Code);

   procedure Add_Error
     (Config : in out Lua_Config;
      Error_Type : Lua_Conf.Error_Type;
      Message : String);

  -- Simple, Somewhat generic Utils

   Stack_Top : constant Integer := -1;
   Newline : constant String := (1 => Standard.Ascii.LF);

   procedure For_Each_Part
     (Source : String;
      Separator : Character;
      Action : not null access procedure (Part : String));

   --  Utils
   procedure Expect (State : Lua_State; Expected_Type : Lua_Type);

   procedure Lift_Nested_Var_To_Top
     (State : Lua_State;
      Field_Path : String);
end Lua_Conf;
