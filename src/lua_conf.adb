with Ada.Text_IO; use Ada.Text_IO;
with Ada.Exceptions; use Ada.Exceptions;
with System;
with Interfaces.C.Strings; use Interfaces.C.Strings;
with Interfaces.C; use Interfaces.C;

with Ada.Strings; use Ada.Strings;
with Ada.Strings.Fixed; use Ada.Strings.Fixed;

package body Lua_Conf is

   -------------
   --  State  --
   -------------
   function State (Config : Lua_Config) return Lua_State
     is (Config.State);
   
   ---------------
   -- Open_Libs --
   ---------------

   procedure Open_Libs (State : Lua_State) is
   begin
      Lua.Open_Libs(State);
   end Open_Libs;

   ----------------------
   -- Open_Non_Io_Libs --
   ----------------------

   procedure Open_Non_Io_Libs (State : Lua_State)
   is
      function Open_Coroutine (State : Lua_State) return int
      with Import, Convention => C, Link_Name => "luaopen_coroutine";

      function Open_String (State : Lua_State) return int
      with Import, Convention => C, Link_Name => "luaopen_string";

      function Open_Table (State : Lua_State) return int
      with Import, Convention => C, Link_Name => "luaopen_table";

      function Open_UTF8 (State : Lua_State) return int
      with Import, Convention => C, Link_Name => "luaopen_utf8";

      function Open_Math (State : Lua_State) return int
      with Import, Convention => C, Link_Name => "luaopen_math";

      type Lua_CFunction_ptr is access function
        (State : Lua_State)
        return int
      with Convention => C;

      type Lib is record
         Name : chars_ptr;
         Func : Lua_CFunction_ptr;
      end record;

      procedure Require
        (State : Lua_State;
         Name : chars_ptr;
         Func : Lua_CFunction_ptr;
         Global : int := 1)
      with Import, Convention => C, Link_Name => "luaL_requiref";

      Libs : array (1..5) of Lib :=
        ((New_String ("coroutine"), Open_Coroutine'Access),
         (New_String ("string"), Open_String'Access),
         (New_String ("table"), Open_Table'Access),
         (New_String ("utf8"), Open_UTF8'Access),
         (New_String ("math"), Open_Math'Access));

   begin
      for Lib of Libs loop
         Require (State, Lib.Name, Lib.Func);
         Lua.Pop (State);
         Free (Lib.Name);
      end loop;
   end Open_Non_Io_Libs;

   ----------
   -- Load --
   ----------

   function Load
     (File_Path : String;
      Prerun : not null access procedure
        (State : Lua_State) := Open_Non_Io_Libs'Access;
      Postrun : not null access procedure
        (State : Lua_State) := Nothing'Access)
     return Lua_Config
   is
      Result : Lua_Config :=
        (State => Lua.New_State,
         Error_Vect => Error_Messages.Empty_Vector);

      Load_Status : Lua.Lua_Return_Code;
      Run_Status : Lua.Lua_Return_Code;
   begin
      Load_Status := Lua.Load_File
        (State => Result.State,
         Filename => File_Path);

      if Load_Status /= LUA_OK then
         declare
            Message : constant String := Create_Message
              (Config => Result,
               File_Path => File_Path,
               Code => Load_Status);
         begin
            raise Load_Error with Message;
         end;
      end if;

      begin
         Prerun (State => Result.State);
      exception
         when Error : others =>
            raise Load_Error with
              "Prerun failed: " & Exception_Message (Error);
      end;

      Run_Status := Lua.PCall (State => Result.State);

      if Run_Status /= LUA_OK then
         declare
            Message : constant String := Create_Message
              (Config => Result,
               File_Path => File_Path,
               Code => Run_Status);
         begin
            raise Load_Error with Message;
         end;
      end if;

      begin
         Postrun (State => Result.State);
      exception
         when Error : others =>
            raise Load_Error with
              "Prerun failed: " & Exception_Message (Error);
      end;

      return Result;
   end Load;

   -----------
   -- Close --
   -----------

   procedure Close (Config : in out Lua_Config) is
   begin
      Error_Messages.Clear (Container => Config.Error_Vect);
      Lua.Close (State => Config.State);
   end Close;

   ------------
   -- Errors --
   ------------

   function Errors (Config : Lua_Config) return Error_Messages.Vector is
   begin
      -- TODO: Should we copy that or not?
      return Config.Error_Vect;
   end Errors;

   -------------
   --  Clear  --
   -------------

   procedure Clear
     (Config : in out Lua_Config;
      Name : String)
   is
      Last_Dot_Index : constant Natural := Index (Name, ".", Backward);
      Is_Nested_Table : constant Boolean := Last_Dot_Index /= 0;
   begin
      if Is_Nested_Table then
         declare
            Table : constant String := Name (Name'First .. Last_Dot_Index - 1);
            Field : constant String := Name (Last_Dot_Index + 1 .. Name'Last);
         begin
            Lift_Nested_Var_To_Top (Config.State,Table);

            --  Path goes to some weird place and we can only clear
            --  a field when we get a proper table
            if Get_Type (Config.State, Stack_Top) /= LUA_TTABLE then
               --  remove bad leftover value
               Lua.Pop (Config.State);
               Add_Error
                 (Config, Type_Mismatch,
                  "Cannot clear '" & Name & "'. '" & Table & "' is not a table.");
               return;
            end if;
            
            Lua.Push (Config.State);
            Lua.Set_Field (Config.State, Stack_Top - 1, Field);
         end;
      else
         Lua.Push (Config.State);
         Lua.Set_Global (Config.State, Name);
      end if;
   exception
      --  this happens when the path is not a nested table
      when Error : Lua_Type_Error => 
         Add_Error
           (Config, Type_Mismatch,
            "Cannot clear '" & Name & "'. Reason: " & Exception_Message (Error));
   end Clear;

   ------------------------
   -- Get Implementation --
   ------------------------

   procedure Lift_Nested_Var_To_Top
     (State : Lua_State;
      Field_Path : String)
   is
      Part_Number : Natural := 0;
      Is_Nested : constant Boolean := Index (Field_Path, ".") /= 0;

      procedure Rotate
        (State : Lua_State;
         From : Lua_Index;
         To : Lua_Index)
      with
        Import,
        Convention => C,
        Link_Name => "lua_rotate";

      procedure Assert_Top_Is_Table is
      begin
         if Is_Nested and then Get_Type (State, Stack_Top) /= LUA_TTABLE then
            Lua.Pop (State);
            raise Lua_Type_Error with
              "Part" & Natural'Image (Part_Number) & " of the path '"
              & Field_Path & "' is a non-table value.";
         end if;
      end Assert_Top_Is_Table;

      procedure Push_Table (Name : String) is
      begin
         Part_Number := Part_Number + 1;

         if Part_Number = 1 then
            Lua.Get_Global (State, Name);
            Assert_Top_Is_Table;
         else
            -- If we index into it, it has to be a table
            Assert_Top_Is_Table;
            Lua.Get_Field (State, Stack_Top, Name);

            -- Remove (Stack_Top - 1), the old table
            -- we want to leave the stack nice and clean
            Rotate (State, From => Stack_Top - 1, To => Stack_Top);
            Lua.Pop (State);
         end if;
      end Push_Table;
   begin
      For_Each_Part
        (Source => Field_Path,
         Separator => '.',
         Action => Push_Table'Access);
   end Lift_Nested_Var_To_Top;

   function Get_Generic
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Result_Type := Default_Value;
      Mandatory : Boolean := False)
     return Result_Type
   is
   begin

      Lift_Nested_Var_To_Top (Config.State, Field_Path);

      declare
         Result : constant Result_Type := Extract_From_Lua_Top(State => Config.State);
      begin
         return Result;
      end;
   exception
      when Error : Lua_Type_Error =>
         --  Lift_Nested_Var_To_Top pops on error

         if Mandatory then
            raise Mandatory_Option_Missing with
              "Cannot read mandatory '" & Field_Path & "'. "
              & Exception_Message (Error);
         else
            Add_Error
              (Config, Type_Mismatch,
               "Cannot read '" & Field_Path & "'. "
                 & Exception_Message (Error) & " Using default value instead.");
            return Default;
         end if;
   end Get_Generic;

   ----------------------------
   -- Extraction for Vectors --
   ----------------------------

   function Extract_Collection_From_Lua_Top
     (State : Lua_State)
     return Collection
   is
      Result : Collection := Empty_Vector;
      Table_Index : constant Lua_Index := Get_Top (State);
      Index : Natural := 0;
   begin
      Lua.Push (State); -- first "key"

      while Lua.Next (State, Table_Index) loop
         Index := Index + 1;
         -- next pushes the key at (-2) and the value at (-1)
         declare
            Value : constant Result_Type := Extract_From_Lua_Top (State);
         begin
            Append (Container => Result, New_Item => Value);
         end;

         -- remove value, keep the key for next to Continue
         Lua.Pop (State);
      end loop;

      -- Remove the last key
      Lua.Pop (State);

      return Result;
   exception
      when Error : Lua_Type_Error =>
         -- Remove key and value
         Lua.Pop (State => State, N => 2);

         raise Lua_Type_Error with
           "While traversing the table an error at Key number" &
           Integer'Image(Index) & " occurred. " & Exception_Message (Error);
   end Extract_Collection_From_Lua_Top;


   -----------
   -- Utils --
   -----------

   function Create_Message
     (Config : in out Lua_Config;
      File_Path : String;
      Code : Lua_Return_Code)
     return String
   is
      Lua_Msg_Head : constant String := "Lua Message: ";

      Ada_Msg : constant String :=
        (case Code is
            when LUA_YIELD => "Config file Yielded.",
            when LUA_ERRRUN => "A runtime error occurred in the config file.",
            when LUA_ERRSYNTAX => "Syntax error in config file.",
            when LUA_ERRMEM => "Config ran out of memory.",
            when LUA_ERRGCMM => "Config caused an GC error.",
            when LUA_ERRERR => "Config caused an unexpected error.",
            when LUA_ERRFILE => "Config could not be opened.",
            when LUA_OK => "INTERNAL ERROR: Return code was LUA_OK when it shouldn't have been.");

      Lua_Msg : constant String := To_Ada (Config.State, Stack_Top);
   begin
      Lua.Pop (State => Config.State);
      return File_Path & ": " & Ada_Msg & Newline &
             Lua_Msg_Head & Lua_Msg & Newline;
   end;

   procedure Collect_Lua_Error
     (Config : in out Lua_Config;
      File_Path : String;
      Code : Lua_Return_Code) is
   begin
      if Code /= Lua.LUA_OK then
         declare
            Message : constant String := Create_Message
              (File_Path => File_Path,
               Config => Config,
               Code => Code);
         begin
            Add_Error (Config, Lua_Error, Message);
         end;
      end if;
   end;

   procedure Add_Error
     (Config : in out Lua_Config;
      Error_Type : Lua_Conf.Error_Type;
      Message : String)
   is
      Error : constant Error_Message :=
        (Error_Type => Error_Type,
         Message_Length => Message'Length,
         Message => Message);
   begin
      Error_Messages.Append
        (Container => Config.Error_Vect,
         New_Item => Error);
   end Add_Error;

   ----------------------------
   -- Somewhat generic Utils --
   ----------------------------

   -- TODO: this should only allow a-zA-Z0-9_- for chars
   function Is_Path
     (Path : String;
      Separator : Character := '.')
     return Boolean
   is
      type State_Type is (Sep, Char);
      State : State_Type := Sep;
      Matches : Boolean := True;

      Last : Character;
   begin
      for Current of Path loop
         case State is
            when Char =>
               if Current = Separator then
                  State := Sep;
               end if;

            when Sep =>
               if Current = Separator then
                  Matches := False;
                  exit;
               else
                  State := Char;
               end if;
         end case;

         Last := Current;
      end loop;

      return Matches and Last /= Separator;
   end Is_Path;

   procedure For_Each_Part
     (Source : String;
      Separator : Character;
      Action : not null access procedure (Part : String))
     --  Run Action for each Substring of Source,
     --  which are separated by Separator.
     --  Example: For_Each_Part("..aa.bb..c..", '.', Action)
     --  Will run Action with "", "", "aa", "bb", "", "c", "",""
     --  For N separators Action will be run exactly N+1 times
     --  If Source is "" then Action ("") will be run.
     --
     --  @param Source The String which will be separated by Separator
     --  @param Separator The Separator used to divide Source into parts
     --  @param Action The Procedure which is run on every part
   is

      type State_Type is (Start, Separators, Characters, Done);
      -- Start: Start State
      --    Source is empty => Done
      --    Read Separator => Action ("") then Separators
      --    Read Character => Do Nothing then Characters
      --
      -- Separators: Currently reading Seprators, so emit Action ("")
      --     At End => Action ("") then Done
      --     Read Separator => Action ("") then Separators
      --     Read Character => Do Nothing then Characters
      --
      -- Characters: Currently reading Characters, so wait till Seperator
      --             or Stirng end to spill them out in one Slice
      --     At End => Spill Prevous_Seperator to String end then Done
      --     Read Spearator => Spill Current Part then Separators
      --     Read Character => Do Nothing then Characters
      --
      -- Done: End State

      State : State_Type := Start;

      Previous_Separator : Integer := Source'First - 1;

      At_End : Boolean := False;
      At_Separator : Boolean := False;

      Index : Integer := Source'First;
   begin

     -- Implementation as FSM
      while State /= Done loop
         At_End := Index = Source'Last + 1;

         if not At_End then
            At_Separator := Source(Index) = Separator;
         end if;

         case State is
            when Start =>
               if Source'Length = 0 then
                  Action ("");
                  State := Done;
               elsif At_Separator then
                  Action ("");
                  State := Separators;
               else -- at character
                  State := Characters;
               end if;

            when Separators =>
               if At_End then
                  Action ("");
                  State := Done;
               elsif At_Separator then
                  Action ("");
                  -- Stay in Characters
               else -- At Character
                  State := Characters;
               end if;

            when Characters =>
               if At_End then
                  Action (Source (Previous_Separator + 1 .. Source'Last));
                  State := Done;
               elsif At_Separator then
                  Action (Source (Previous_Separator + 1 .. Index - 1));
                  State := Separators;
               else -- at character
                  null; -- stay in Characters
               end if;

            when Done => null;
         end case;

         if At_Separator then
            Previous_Separator := Index;
         end if;

         Index := Index + 1;
      end loop;
   end For_Each_Part;


   ----------------------------------
   -- Boring Instances and renames --
   ----------------------------------

   -------------------
   -- Get Instances --
   -------------------

   function Get_Bool is new Get_Generic (Boolean, False);
   function Get_Bools is new Get_Generic (Booleans.Vector, Booleans.Empty_Vector);

   function Get_String is new Get_Generic (String, "");
   function Get_Strings is new Get_Generic (Strings.Vector, Strings.Empty_Vector);

   function Get_Integer is new Get_Generic (Integer, 0);
   function Get_Integers is new Get_Generic (Integers.Vector, Integers.Empty_Vector);

   function Get_Float is new Get_Generic (Float, 0.0);
   function Get_Floats is new Get_Generic (Floats.Vector, Floats.Empty_Vector);

   function Get_Long_Float is new Get_Generic (Long_Float, 0.0);
   function Get_Long_Floats is new Get_Generic (Long_Floats.Vector, Long_Floats.Empty_Vector);

   function Get_Lua_Function is new Get_Generic (Lua_Function, null);
   function Get_Lua_Functions is new Get_Generic (Lua_Functions.Vector, Lua_Functions.Empty_Vector);

   -----------------
   -- Get renames --
   -----------------

   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Boolean := False;
      Mandatory : Boolean := False)
     return Boolean renames Get_Bool;

   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Booleans.Vector := Booleans.Empty_Vector;
      Mandatory : Boolean := False)
     return Booleans.Vector renames Get_Bools;

   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : String := "";
      Mandatory : Boolean := False)
     return String renames Get_String;

   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Strings.Vector := Strings.Empty_Vector;
      Mandatory : Boolean := False)
     return Strings.Vector renames Get_Strings;

   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Integer := 0;
      Mandatory : Boolean := False)
     return Integer renames Get_Integer;

   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Integers.Vector := Integers.Empty_Vector;
      Mandatory : Boolean := False)
     return Integers.Vector renames Get_Integers;

   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Float := 0.0;
      Mandatory : Boolean := False)
     return Float renames Get_Float;

   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Floats.Vector := Floats.Empty_Vector;
      Mandatory : Boolean := False)
     return Floats.Vector renames Get_Floats;

   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Long_Float := 0.0;
      Mandatory : Boolean := False)
     return Long_Float renames Get_Long_Float;

   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Long_Floats.Vector := Long_Floats.Empty_Vector;
      Mandatory : Boolean := False)
     return Long_Floats.Vector renames Get_Long_Floats;

   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Lua_Function := null;
      Mandatory : Boolean := False)
     return Lua_Function renames Get_Lua_Function;

   function Get
     (Config : in out Lua_Config;
      Field_Path : String;
      Default : Lua_Functions.Vector := Lua_Functions.Empty_Vector;
      Mandatory : Boolean := False)
     return Lua_Functions.Vector renames Get_Lua_Functions;


   ------------------------------------------------------------
   -- Boring Instance and Renamings for Extract_From_Lua_Top --
   ------------------------------------------------------------

   -- Extract_From_Lua_Top for scalar (Bool, Int, Float, Double, String, Lua_Function)
   -- are wrappers around To_Ada
   --
   -- The Vector counterpart are instances of the generic above

   procedure Expect (State : Lua_State; Expected_Type : Lua_Type)
   is
     Top_Type : constant Lua_Type :=  Get_Type (State, Stack_Top);
   begin
      if Top_Type /= Expected_Type then
         raise Lua_Type_Error with
           "Wrong Type: Got " & Lua_Type'Image (Top_Type) &
           ", but expected " & Lua_Type'Image (Expected_Type) & ".";
      end if;
   end Expect;

   function Extract_From_Lua_Top
     (State : Lua_State)
     return Boolean
   is
   begin
      Expect (State, LUA_TBOOLEAN);
      return Lua.To_Ada(State, Stack_Top);
   end Extract_From_Lua_Top;

   function Extract_Booleans is new Extract_Collection_From_Lua_Top
     (Boolean, Booleans.Vector, Booleans.Empty_Vector, Booleans.Append, Extract_From_Lua_Top);

   function Extract_From_Lua_Top (State : Lua_State) return Booleans.Vector
   renames Extract_Booleans;



   function Extract_From_Lua_Top (State : Lua_State) return String
   is
   begin
      Expect (State, LUA_TSTRING);
      return Lua.To_Ada(State, Stack_Top);
   end Extract_From_Lua_Top;

   function Extract_Strings is new Extract_Collection_From_Lua_Top
     (String, Strings.Vector, Strings.Empty_Vector, Strings.Append, Extract_From_Lua_Top);

   function Extract_From_Lua_Top (State : Lua_State) return Strings.Vector
   renames Extract_Strings;



   function Extract_From_Lua_Top (State : Lua_State) return Integer
   is
      Lua_Result : Lua_Integer;
   begin
      Expect (State, LUA_TNUMBER);
      Lua_Result := Lua.To_Ada(State, Stack_Top);
      return Integer (Lua_Result);
   exception
      when Constraint_Error => raise Lua_Type_Error with "Integer out of range";
   end Extract_From_Lua_Top;

   function Extract_Integers is new Extract_Collection_From_Lua_Top
     (Integer, Integers.Vector, Integers.Empty_Vector, Integers.Append, Extract_From_Lua_Top);

   function Extract_From_Lua_Top (State : Lua_State) return Integers.Vector
   renames Extract_Integers;



   function Extract_From_Lua_Top (State : Lua_State) return Float
   is
      Lua_Result : Lua_Float;
   begin
      Expect (State, LUA_TNUMBER);
      Lua_Result := Lua.To_Ada(State, Stack_Top);
      return Float (Lua_Result);
   exception
      when Constraint_Error => raise Lua_Type_Error with "Float out of range";
   end Extract_From_Lua_Top;

   function Extract_Floats is new Extract_Collection_From_Lua_Top
     (Float, Floats.Vector, Floats.Empty_Vector, Floats.Append, Extract_From_Lua_Top);

   function Extract_From_Lua_Top (State : Lua_State) return Floats.Vector
   renames Extract_Floats;



   function Extract_From_Lua_Top (State : Lua_State) return Long_Float
   is
      Lua_Result : Lua_Float;
   begin
      Expect (State, LUA_TNUMBER);
      Lua_Result := Lua.To_Ada(State, Stack_Top);
      return Long_Float (Lua_Result);
   exception
      when Constraint_Error => raise Lua_Type_Error with "Long_Float out of range";
   end Extract_From_Lua_Top;

   function Extract_Long_Floats is new Extract_Collection_From_Lua_Top
     (Long_Float, Long_Floats.Vector, Long_Floats.Empty_Vector, Long_Floats.Append, Extract_From_Lua_Top);

   function Extract_From_Lua_Top (State : Lua_State) return Long_Floats.Vector
   renames Extract_Long_Floats;



   function Extract_From_Lua_Top (State : Lua_State) return Lua_Function
   is
   begin
      Expect (State, LUA_TFUNCTION);
      return Lua.To_Ada(State, Stack_Top);
   end Extract_From_Lua_Top;

   function Extract_Lua_Functions is new Extract_Collection_From_Lua_Top
     (Lua_Function, Lua_Functions.Vector, Lua_Functions.Empty_Vector, Lua_Functions.Append, Extract_From_Lua_Top);

   function Extract_From_Lua_Top (State : Lua_State) return Lua_Functions.Vector
   renames Extract_Lua_Functions;

end Lua_Conf;
