# ada-lua-conf
Use a .lua script as a config file

# Usage Example
Look at test/run_test.adb for a little usage example.
It loads test/test.lua and extracts some values.

For more information consult the src/lua_conf.ads comments.

TODO: extend this to a proper unit test

# Build
The project assumes that the ada-lua source is located at ../ada-lua/

Run `gprbuild -P library.gpr` to build the library.

Run `gprbuild -P test.gpr` to build the "test".

Run `gprbuild -P build.gpr` to build both.

As of now (May 2015) you can still use `gnatmake` instead, but this is deprecated.
